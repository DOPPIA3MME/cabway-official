//
//  NotificationView.swift
//  CabwayOfficial WatchKit Extension
//
//  Created by Maurizio Minieri on 14/01/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Your taxi is here")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
