//
//  HostingController.swift
//  CabwayOfficial WatchKit Extension
//
//  Created by Maurizio Minieri on 14/01/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ContentView> {
	override var body: ContentView {
		
		return ContentView()
	}
	
	/*
	WKHostingController<Wave> {
	override var body: Wave {
	return Wave(graphWidth: 1, amplitude: 0.05)
	}*/
	
}

