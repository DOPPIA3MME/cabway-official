//
//  ContentView.swift
//  CabwayOfficial WatchKit Extension
//
//  Created by Maurizio Minieri on 14/01/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import SwiftUI
import CoreLocation
//import MapKit

/*
class ExampleDataHolder: ObservableObject {
	@Published var text: String = ""
}

struct CommonAncestorOfTheViews: View {
	var body: some View {
		ContentView().environmentObject(ExampleDataHolder())
	}
}


*/
struct ContentView: View {
	@State private var from: String = "Your position"
	@State private var to: String = "Via Giacomo Leopardi"
	var navigationTitle = "Cabway"
	
	//@State var description: String = "MELA"
	
	
	//BISOGNA VEDERE
	// @EnvironmentObject var dataHolder: ExampleDataHolder
	
	//data che è visibile in tutte le views nell'intera app
	//@Environment var element: String
	
	//oggetti osservabili che annunciano automaticamente quando si verificano cambiamenti
	//@Published var element: String
	
	
	
	var disableButton: Bool {
		from.count < 6 || to.count < 6
	}
	
	var body: some View {
		
		VStack {
			//TextField("From", text: $from)
			//.padding(.top, 40.0)
			
			Button(action: {}, label: {
				NavigationLink(destination: Favourite()) {
					
					HStack {
						     //DOVE VA MESSO???	//ContentView().environmentObject(ExampleDataHolder())
						Image(systemName: "location.fill")
						TextField("From", text: $from)
					
					}
				}.navigationBarTitle("Cabway")
				
				
			}).padding(.top, 40.0)
			
			
			Button(action: {
				
				
			}, label: {
				NavigationLink(destination: Favourite()) {
					TextField("To", text: $to)
					
					
				}.navigationBarTitle("Cabway")
				
			})
			
			
			Button(action: {}, label: {
				
				//Mostro la struct Book
				NavigationLink(destination: Book()) {
					Text("GO")
						.fontWeight(.bold)
						.multilineTextAlignment(.center)
						.frame(width: 80.0, height: 80.0)
				}.navigationBarTitle("Cabway").accentColor(.yellow)
				
				//Per richiamare una funzione si fa cosi
			})//.onAppear{getPosition(userLatitude: "37.78583400",userLongitude: userLongitude)}
				
				
				.font(.title)
				.background(disableButton ? Color.gray : Color.yellow)
				.disabled(disableButton ? true : false)
				.opacity(disableButton ? 0.15 : 1)
				.foregroundColor(disableButton ? .gray : .black)
				
				.cornerRadius(50)
				.frame(width: 80.0, height: 80.0)
		}
		
		
		
		
	}
	
	

}


//serve per mostrare nella canvas (in alto a destra), IN TEMPO REALE, la schermata a cui stai lavorando, molto molto comoda.
struct ContentView_Preview: PreviewProvider {
	static var previews: some View {
		ContentView()
	}
}



//MARK: - View per la posizione
struct LocationTestView: View {
	
	@ObservedObject var lm = LocationManager()
	
	var latitude: String  { return("\(lm.location?.latitude ?? 0)") }
	var longitude: String { return("\(lm.location?.longitude ?? 0)") }
	var placemark: String { return("\(lm.placemark?.description ?? "XXX")") }
	var status: String    { return("\(String(describing: lm.status))") }
	
	
	var body: some View {
		
		VStack {
			List{
				Text("Latitude: \(self.latitude)")
				Text("Longitude: \(self.longitude)")
				Text("Placemark: \(self.placemark)")
				Text("Status: \(self.status)")
			}
		}
	}
}

struct LocationTestView_Preview: PreviewProvider {
	static var previews: some View {
		LocationTestView()
	}
}


//MARK: - View per la prenotazione del taxi
struct Book: View {
	
	var body: some View {
		
		VStack(alignment: .center){
			
			HStack(alignment: .bottom){
				
				VStack(alignment: .leading){
					
					Image("Taxi")
						.resizable()
						.scaledToFit()
						.frame(width: 70.0, height: 70.0)
						.clipShape(Circle())
						//.shadow(radius: 10)
						.overlay(Circle().stroke(Color.white, lineWidth: 1).imageScale(/*@START_MENU_TOKEN@*/.medium/*@END_MENU_TOKEN@*/))
					.transition(AnyTransition.opacity.combined(with: .slide))
					
					
					
					Text("4 E")
						.multilineTextAlignment(.leading)
					Text("1.2 KM")
						.multilineTextAlignment(.leading)
					
				}
				Spacer()
				VStack(alignment: .center){
					Text("3")
						.fontWeight(.semibold)
						.multilineTextAlignment(.trailing)
						
						.frame(width: 70.0, height: 120.0)
						.font(.system(size: 80))
					
					Text("MINS")
						
					
				}
				
			}
			
			
			VStack(alignment: .center){
				Spacer()
				Button(action: {},label:{
					VStack {
						NavigationLink(destination: Info()) {
							Text("Confirm")
								.font(.caption)
								.fontWeight(.regular)
								.foregroundColor(Color.black)
								.multilineTextAlignment(.center)
								.lineLimit(nil)
								.padding()
								.frame(height: nil)
						}
					}
					
				})
					.frame(height: 40.0)
					.background(Color.yellow)
					.cornerRadius(10)
			}
			
		}
	}
}

struct Book_Preview: PreviewProvider {
	static var previews: some View {
		Group{
			Book()
			Book()
				.previewDevice("Apple Watch Series 3 - 38mm")
		}
	}
}



//MARK: - View per la prenotazione del taxi
struct Favourite: View {
	@State private var home: String = ""
	@State private var work: String = ""
	@State private var choose: String = ""
	@State public var testoscelto: String = ""

	
	
	
	@Environment(\.presentationMode) var mode: Binding<PresentationMode>
	
	var cv: ContentView = ContentView()
	//@EnvironmentObject var dataHolder: ExampleDataHolder
	
	var body: some View {
		
		VStack(alignment: .center) {
			
			HStack{
				
				Button(action: {
					self.mode.wrappedValue.dismiss()
					
					
					//self.dataHolder.text = "HOME"
					
				}) {
					
						HStack {
							
							Image(systemName: "house.fill")
								.foregroundColor(Color.black)
							
							Text("Home")
								.foregroundColor(Color.black)
								.font(.system(size: 8))
						}
						
					
				}
				.frame(height: 45.0)
				.background(Color.yellow)
				.cornerRadius(10)
				
				
				
				Button(action: {self.mode.wrappedValue.dismiss()}) {
					
						HStack {
							Image(systemName: "bag.fill")
								.foregroundColor(Color.black)
							Text("Work")
								.foregroundColor(Color.black)
								.font(.system(size: 8))
						}
					
				}
				.frame(height: 45.0)
				.background(Color.yellow)
				.cornerRadius(10)
			}
			.frame(height: nil)
			TextField("Choose", text: $choose)
			List{
				
				Button(action: {self.mode.wrappedValue.dismiss()}){
				
					Text("via cercata 1")
				}
				Button(action: {self.mode.wrappedValue.dismiss()}){
					
					Text("via cercata 2")
				}
				Button(action: {self.mode.wrappedValue.dismiss()}){
					
					Text("via cercata 3")
				}
				
			}
		}
	}
}

struct Favourite_Preview: PreviewProvider {
	static var previews: some View {
		Group{
			Favourite()
			Favourite()
				.previewDevice("Apple Watch Series 3 - 38mm")
		}
	}
}

//MARK: - View finale
struct Info: View {
	@State private var animationAmount: CGFloat = 1
	
	
	var body: some View {
		VStack(alignment: .center){
			
			
			Image ("Check")
				.resizable()
				.padding(.bottom)
				.scaledToFit()
				.frame(width: 100.0, height: 100.0)
				.clipShape(Circle())
				.shadow(radius: 30)
			
			VStack(spacing: 5.0) {
				Text("Your taxy is coming, thank you.")
					.multilineTextAlignment(.center)
			}
		}
	}
}

struct Info_Preview: PreviewProvider {
	static var previews: some View {
		Info()
	}
}



/*
struct MapView: WKInterfaceObjectRepresentable {

@ObservedObject private var locationManager = LocationManager()

func makeWKInterfaceObject(context: WKInterfaceObjectRepresentableContext<MapView>) -> WKInterfaceMap {

return WKInterfaceMap()
}


func updateWKInterfaceObject(_ map: WKInterfaceMap, context: WKInterfaceObjectRepresentableContext<MapView
>) {




let span = MKCoordinateSpan(latitudeDelta: 0.005,
longitudeDelta: 0.005
)
let region = MKCoordinateRegion(
center: CLLocationCoordinate2D(latitude: locationManager.lastLocation?.coordinate.latitude ?? 0, longitude: locationManager.lastLocation?.coordinate.longitude ?? 0)  ,
span: span)
map.setShowsUserLocation(true)
map.setRegion(region)


}

}
*/

/*ANCORA DA IMPLEMENTARE MANCA QUESTO IN APPDELEGATE:"
import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        willPresent notification: UNNotification,
        withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions)
        -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UNUserNotificationCenter.current().delegate = self
        return true
    }

    // ... omitted
}"*/

struct NotificationsSend: View {
    
    func setNotification() -> Void {
        let manager = LocalNotificationManager()
        manager.addNotification(title: "This is a test reminder")
        manager.schedule()
    }
    
    var body: some View {
        VStack {
            Text("Notification Demo")
            Button(action: { self.setNotification() }) {
                Text("Set Notification!")
            }
        }
    }
    
}

struct NotificationsSend_Previews: PreviewProvider {
    static var previews: some View {
        NotificationsSend()
    }
}


