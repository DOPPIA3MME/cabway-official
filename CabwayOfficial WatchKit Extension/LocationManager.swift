//
//  LocationManager.swift
//  CabwayOfficial WatchKit Extension
//
//  Created by Maurizio Minieri on 17/01/2020.
//  Copyright © 2020 Maurizio Minieri. All rights reserved.
//

import Foundation
import CoreLocation
import Combine

class LocationManager: NSObject, ObservableObject {
	private let geocoder = CLGeocoder()
	private let locationManager = CLLocationManager()
	let objectWillChange = PassthroughSubject<Void, Never>()
	
	@Published var status: CLAuthorizationStatus? {
		willSet { objectWillChange.send() }
	}
	
	@Published var location: CLLocation? {
		willSet { objectWillChange.send() }
	}
	
	@Published var placemark: CLPlacemark? {
		willSet { objectWillChange.send() }
	}
	
	override init() {
		super.init()
		
		self.locationManager.delegate = self
		self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
		self.locationManager.requestWhenInUseAuthorization()
		self.locationManager.startUpdatingLocation()
	}
	
  //viene chiamata ogni volta che la posizione cambia, serve oper calcolare il placemark
	func geocode() {
		
		guard let location = self.location else { return }
		
		geocoder.reverseGeocodeLocation(location, completionHandler: { (places, error) in
			if error == nil {
				self.placemark = places?[0]
			} else {
				self.placemark = nil
			}
		})
		
	}
	
	
}


extension LocationManager: CLLocationManagerDelegate {
	//quando viene data l'autorizzazione
	func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
		self.status = status
	}
	
	//quando viene aggiornata la location
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		guard let location = locations.last else { return }
		self.location = location
		self.geocode()
	}
}


//rendo la latitudine e longitudine visibili, non potrei usare lm.location.longitude e latitude altrimenti
extension CLLocation {
	var latitude: Double {
		return self.coordinate.latitude
	}
	
	var longitude: Double {
		return self.coordinate.longitude
	}
}

