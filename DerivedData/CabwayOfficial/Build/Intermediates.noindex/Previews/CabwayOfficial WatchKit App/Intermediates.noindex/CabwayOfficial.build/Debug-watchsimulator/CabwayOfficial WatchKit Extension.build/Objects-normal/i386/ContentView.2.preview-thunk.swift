@_private(sourceFile: "ContentView.swift") import CabwayOfficial_WatchKit_Extension
import CoreLocation
import SwiftUI
import SwiftUI

extension NotificationsSend_Previews {
    @_dynamicReplacement(for: previews) private static var __preview__previews: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 446)
        AnyView(NotificationsSend())
#sourceLocation()
    }
}

extension NotificationsSend {
    @_dynamicReplacement(for: body) private var __preview__body: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 434)
        AnyView(VStack {
            Text(__designTimeString("#4421.[12].[1].property.[0].[0].arg[0].value.[0].arg[0].value.[0].value", fallback: "Notification Demo"))
            Button(action: { self.setNotification() }) {
                Text(__designTimeString("#4421.[12].[1].property.[0].[0].arg[0].value.[1].arg[1].value.[0].arg[0].value.[0].value", fallback: "Set Notification!"))
            }
        })
#sourceLocation()
    }
}

extension NotificationsSend {
    @_dynamicReplacement(for: setNotification()) private func __preview__setNotification() -> Void {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 428)
        let manager = LocalNotificationManager()
        manager.addNotification(title: __designTimeString("#4421.[12].[0].[1].modifier[0].arg[0].value.[0].value", fallback: "This is a test reminder"))
        manager.schedule()
#sourceLocation()
    }
}

extension Info_Preview {
    @_dynamicReplacement(for: previews) private static var __preview__previews: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 363)
		AnyView(Info())
#sourceLocation()
    }
}

extension Info {
    @_dynamicReplacement(for: body) private var __preview__body: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 342)
		AnyView(VStack(alignment: .center){
			
			
			Image (__designTimeString("#4421.[10].[1].property.[0].[0].arg[1].value.[0].arg[0].value.[0].value", fallback: "Check"))
				.resizable()
				.padding(.bottom)
				.scaledToFit()
				.frame(width: __designTimeFloat("#4421.[10].[1].property.[0].[0].arg[1].value.[0].modifier[3].arg[0].value", fallback: 100.0), height: __designTimeFloat("#4421.[10].[1].property.[0].[0].arg[1].value.[0].modifier[3].arg[1].value", fallback: 100.0))
				.clipShape(Circle())
				.shadow(radius: __designTimeInteger("#4421.[10].[1].property.[0].[0].arg[1].value.[0].modifier[5].arg[0].value", fallback: 30))
			
			VStack(spacing: __designTimeFloat("#4421.[10].[1].property.[0].[0].arg[1].value.[1].arg[0].value", fallback: 5.0)) {
				Text(__designTimeString("#4421.[10].[1].property.[0].[0].arg[1].value.[1].arg[1].value.[0].arg[0].value.[0].value", fallback: "Your taxy is coming, thank you."))
					.multilineTextAlignment(.center)
			}
		})
#sourceLocation()
    }
}

extension Favourite_Preview {
    @_dynamicReplacement(for: previews) private static var __preview__previews: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 328)
		AnyView(Group{
			Favourite()
			Favourite()
				.previewDevice(__designTimeString("#4421.[9].[0].property.[0].[0].arg[0].value.[1].modifier[0].arg[0].value.[0].value", fallback: "Apple Watch Series 3 - 38mm"))
		})
#sourceLocation()
    }
}

extension Favourite {
    @_dynamicReplacement(for: body) private var __preview__body: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 259)
		
		AnyView(VStack(alignment: .center) {
			
			HStack{
				
				Button(action: {
					self.mode.wrappedValue.dismiss()
					
					
					//self.dataHolder.text = "HOME"
					
				}) {
					
						HStack {
							
							Image(systemName: __designTimeString("#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[0].arg[1].value.[0].arg[0].value.[0].arg[0].value.[0].value", fallback: "house.fill"))
								.foregroundColor(Color.black)
							
							Text(__designTimeString("#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[0].arg[1].value.[0].arg[0].value.[1].arg[0].value.[0].value", fallback: "Home"))
								.foregroundColor(Color.black)
								.font(.system(size: __designTimeInteger("#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[0].arg[1].value.[0].arg[0].value.[1].modifier[1].arg[0].value.arg[0].value", fallback: 8)))
						}
						
					
				}
				.frame(height: __designTimeFloat("#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[0].modifier[0].arg[0].value", fallback: 45.0))
				.background(Color.yellow)
				.cornerRadius(__designTimeInteger("#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[0].modifier[2].arg[0].value", fallback: 10))
				
				
				
				Button(action: {self.mode.wrappedValue.dismiss()}) {
					
						HStack {
							Image(systemName: __designTimeString("#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[1].arg[1].value.[0].arg[0].value.[0].arg[0].value.[0].value", fallback: "bag.fill"))
								.foregroundColor(Color.black)
							Text(__designTimeString("#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[1].arg[1].value.[0].arg[0].value.[1].arg[0].value.[0].value", fallback: "Work"))
								.foregroundColor(Color.black)
								.font(.system(size: __designTimeInteger("#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[1].arg[1].value.[0].arg[0].value.[1].modifier[1].arg[0].value.arg[0].value", fallback: 8)))
						}
					
				}
				.frame(height: __designTimeFloat("#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[1].modifier[0].arg[0].value", fallback: 45.0))
				.background(Color.yellow)
				.cornerRadius(__designTimeInteger("#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[1].modifier[2].arg[0].value", fallback: 10))
			}
			.frame(height: nil)
			TextField(__designTimeString("#4421.[8].[6].property.[0].[0].arg[1].value.[1].arg[0].value.[0].value", fallback: "Choose"), text: $choose)
			List{
				
				Button(action: {self.mode.wrappedValue.dismiss()}){
				
					Text(__designTimeString("#4421.[8].[6].property.[0].[0].arg[1].value.[2].arg[0].value.[0].arg[1].value.[0].arg[0].value.[0].value", fallback: "via cercata 1"))
				}
				Button(action: {self.mode.wrappedValue.dismiss()}){
					
					Text(__designTimeString("#4421.[8].[6].property.[0].[0].arg[1].value.[2].arg[0].value.[1].arg[1].value.[0].arg[0].value.[0].value", fallback: "via cercata 2"))
				}
				Button(action: {self.mode.wrappedValue.dismiss()}){
					
					Text(__designTimeString("#4421.[8].[6].property.[0].[0].arg[1].value.[2].arg[0].value.[2].arg[1].value.[0].arg[0].value.[0].value", fallback: "via cercata 3"))
				}
				
			}
		})
#sourceLocation()
    }
}

extension Book_Preview {
    @_dynamicReplacement(for: previews) private static var __preview__previews: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 232)
		AnyView(Group{
			Book()
			Book()
				.previewDevice(__designTimeString("#4421.[7].[0].property.[0].[0].arg[0].value.[1].modifier[0].arg[0].value.[0].value", fallback: "Apple Watch Series 3 - 38mm"))
		})
#sourceLocation()
    }
}

extension Book {
    @_dynamicReplacement(for: body) private var __preview__body: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 164)
		
		AnyView(VStack(alignment: .center){
			
			HStack(alignment: .bottom){
				
				VStack(alignment: .leading){
					
					Image(__designTimeString("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[0].arg[1].value.[0].arg[0].value.[0].value", fallback: "Taxi"))
						.resizable()
						.scaledToFit()
						.frame(width: __designTimeFloat("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[0].arg[1].value.[0].modifier[2].arg[0].value", fallback: 70.0), height: __designTimeFloat("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[0].arg[1].value.[0].modifier[2].arg[1].value", fallback: 70.0))
						.clipShape(Circle())
						//.shadow(radius: 10)
						.overlay(Circle().stroke(Color.white, lineWidth: __designTimeInteger("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[0].arg[1].value.[0].modifier[4].arg[0].value.modifier[0].arg[1].value", fallback: 1)).imageScale(/*@START_MENU_TOKEN@*/.medium/*@END_MENU_TOKEN@*/))
					.transition(AnyTransition.opacity.combined(with: .slide))
					
					
					
					Text(__designTimeString("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[0].arg[1].value.[1].arg[0].value.[0].value", fallback: "4 E"))
						.multilineTextAlignment(.leading)
					Text(__designTimeString("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[0].arg[1].value.[2].arg[0].value.[0].value", fallback: "1.2 KM"))
						.multilineTextAlignment(.leading)
					
				}
				Spacer()
				VStack(alignment: .center){
					Text(__designTimeString("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[2].arg[1].value.[0].arg[0].value.[0].value", fallback: "3"))
						.fontWeight(.semibold)
						.multilineTextAlignment(.trailing)
						
						.frame(width: __designTimeFloat("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[2].arg[1].value.[0].modifier[2].arg[0].value", fallback: 70.0), height: __designTimeFloat("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[2].arg[1].value.[0].modifier[2].arg[1].value", fallback: 120.0))
						.font(.system(size: __designTimeInteger("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[2].arg[1].value.[0].modifier[3].arg[0].value.arg[0].value", fallback: 80)))
					
					Text(__designTimeString("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[2].arg[1].value.[1].arg[0].value.[0].value", fallback: "MINS"))
						
					
				}
				
			}
			
			
			VStack(alignment: .center){
				Spacer()
				Button(action: {},label:{
					VStack {
						NavigationLink(destination: Info()) {
							Text(__designTimeString("#4421.[6].[0].property.[0].[0].arg[1].value.[1].arg[1].value.[1].arg[1].value.[0].arg[0].value.[0].arg[1].value.[0].arg[0].value.[0].value", fallback: "Confirm"))
								.font(.caption)
								.fontWeight(.regular)
								.foregroundColor(Color.black)
								.multilineTextAlignment(.center)
								.lineLimit(nil)
								.padding()
								.frame(height: nil)
						}
					}
					
				})
					.frame(height: __designTimeFloat("#4421.[6].[0].property.[0].[0].arg[1].value.[1].arg[1].value.[1].modifier[0].arg[0].value", fallback: 40.0))
					.background(Color.yellow)
					.cornerRadius(__designTimeInteger("#4421.[6].[0].property.[0].[0].arg[1].value.[1].arg[1].value.[1].modifier[2].arg[0].value", fallback: 10))
			}
			
		})
#sourceLocation()
    }
}

extension LocationTestView_Preview {
    @_dynamicReplacement(for: previews) private static var __preview__previews: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 154)
		AnyView(LocationTestView())
#sourceLocation()
    }
}

extension LocationTestView {
    @_dynamicReplacement(for: body) private var __preview__body: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 141)
		
		AnyView(VStack {
			List{
				Text("\(__designTimeString("#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[0].value", fallback: "Latitude: "))\(self.latitude)\(__designTimeString("#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[2].value", fallback: ""))")
				Text("\(__designTimeString("#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[1].arg[0].value.[0].value", fallback: "Longitude: "))\(self.longitude)\(__designTimeString("#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[1].arg[0].value.[2].value", fallback: ""))")
				Text("\(__designTimeString("#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[2].arg[0].value.[0].value", fallback: "Placemark: "))\(self.placemark)\(__designTimeString("#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[2].arg[0].value.[2].value", fallback: ""))")
				Text("\(__designTimeString("#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[3].arg[0].value.[0].value", fallback: "Status: "))\(self.status)\(__designTimeString("#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[3].arg[0].value.[2].value", fallback: ""))")
			}
		})
#sourceLocation()
    }
}

extension LocationTestView {
    @_dynamicReplacement(for: status) private var __preview__status: String {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 136)
 return("\(__designTimeString("#4421.[4].[4].property.[0].[0].[0].[0].value", fallback: ""))\(String(describing: lm.status))\(__designTimeString("#4421.[4].[4].property.[0].[0].[0].[2].value", fallback: ""))")
#sourceLocation()
    }
}

extension LocationTestView {
    @_dynamicReplacement(for: placemark) private var __preview__placemark: String {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 135)
 return("\(__designTimeString("#4421.[4].[3].property.[0].[0].[0].[0].value", fallback: ""))\(lm.placemark?.description ?? "XXX")\(__designTimeString("#4421.[4].[3].property.[0].[0].[0].[2].value", fallback: ""))")
#sourceLocation()
    }
}

extension LocationTestView {
    @_dynamicReplacement(for: longitude) private var __preview__longitude: String {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 134)
 return("\(__designTimeString("#4421.[4].[2].property.[0].[0].[0].[0].value", fallback: ""))\(lm.location?.longitude ?? 0)\(__designTimeString("#4421.[4].[2].property.[0].[0].[0].[2].value", fallback: ""))")
#sourceLocation()
    }
}

extension LocationTestView {
    @_dynamicReplacement(for: latitude) private var __preview__latitude: String {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 133)
 return("\(__designTimeString("#4421.[4].[1].property.[0].[0].[0].[0].value", fallback: ""))\(lm.location?.latitude ?? 0)\(__designTimeString("#4421.[4].[1].property.[0].[0].[0].[2].value", fallback: ""))")
#sourceLocation()
    }
}

extension ContentView_Preview {
    @_dynamicReplacement(for: previews) private static var __preview__previews: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 122)
		AnyView(ContentView())
#sourceLocation()
    }
}

extension ContentView {
    @_dynamicReplacement(for: body) private var __preview__body: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 53)
		
		AnyView(VStack {
			//TextField("From", text: $from)
			//.padding(.top, 40.0)
			
			Button(action: {}, label: {
				NavigationLink(destination: Favourite()) {
					
					HStack {
						     //DOVE VA MESSO???	//ContentView().environmentObject(ExampleDataHolder())
						Image(systemName: __designTimeString("#4421.[2].[4].property.[0].[0].arg[0].value.[0].arg[1].value.[0].arg[1].value.[0].arg[0].value.[0].arg[0].value.[0].value", fallback: "location.fill"))
						TextField(__designTimeString("#4421.[2].[4].property.[0].[0].arg[0].value.[0].arg[1].value.[0].arg[1].value.[0].arg[0].value.[1].arg[0].value.[0].value", fallback: "From"), text: $from)
					
					}
				}.navigationBarTitle(__designTimeString("#4421.[2].[4].property.[0].[0].arg[0].value.[0].arg[1].value.[0].modifier[0].arg[0].value.[0].value", fallback: "Cabway"))
				
				
			}).padding(.top, __designTimeFloat("#4421.[2].[4].property.[0].[0].arg[0].value.[0].modifier[0].arg[1].value", fallback: 40.0))
			
			
			Button(action: {
				
				
			}, label: {
				NavigationLink(destination: Favourite()) {
					TextField(__designTimeString("#4421.[2].[4].property.[0].[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[0].arg[0].value.[0].value", fallback: "To"), text: $to)
					
					
				}.navigationBarTitle(__designTimeString("#4421.[2].[4].property.[0].[0].arg[0].value.[1].arg[1].value.[0].modifier[0].arg[0].value.[0].value", fallback: "Cabway"))
				
			})
			
			
			Button(action: {}, label: {
				
				//Mostro la struct Book
				NavigationLink(destination: Book()) {
					Text(__designTimeString("#4421.[2].[4].property.[0].[0].arg[0].value.[2].arg[1].value.[0].arg[1].value.[0].arg[0].value.[0].value", fallback: "GO"))
						.fontWeight(.bold)
						.multilineTextAlignment(.center)
						.frame(width: __designTimeFloat("#4421.[2].[4].property.[0].[0].arg[0].value.[2].arg[1].value.[0].arg[1].value.[0].modifier[2].arg[0].value", fallback: 80.0), height: __designTimeFloat("#4421.[2].[4].property.[0].[0].arg[0].value.[2].arg[1].value.[0].arg[1].value.[0].modifier[2].arg[1].value", fallback: 80.0))
				}.navigationBarTitle(__designTimeString("#4421.[2].[4].property.[0].[0].arg[0].value.[2].arg[1].value.[0].modifier[0].arg[0].value.[0].value", fallback: "Cabway")).accentColor(.yellow)
				
				//Per richiamare una funzione si fa cosi
			})//.onAppear{getPosition(userLatitude: "37.78583400",userLongitude: userLongitude)}
				
				
				.font(.title)
				.background(disableButton ? Color.gray : Color.yellow)
				.disabled(disableButton ? __designTimeBoolean("#4421.[2].[4].property.[0].[0].arg[0].value.[2].modifier[2].arg[0].value.then", fallback: true) : __designTimeBoolean("#4421.[2].[4].property.[0].[0].arg[0].value.[2].modifier[2].arg[0].value.else", fallback: false))
				.opacity(disableButton ? __designTimeFloat("#4421.[2].[4].property.[0].[0].arg[0].value.[2].modifier[3].arg[0].value.then", fallback: 0.15) : __designTimeInteger("#4421.[2].[4].property.[0].[0].arg[0].value.[2].modifier[3].arg[0].value.else", fallback: 1))
				.foregroundColor(disableButton ? .gray : .black)
				
				.cornerRadius(__designTimeInteger("#4421.[2].[4].property.[0].[0].arg[0].value.[2].modifier[5].arg[0].value", fallback: 50))
				.frame(width: __designTimeFloat("#4421.[2].[4].property.[0].[0].arg[0].value.[2].modifier[6].arg[0].value", fallback: 80.0), height: __designTimeFloat("#4421.[2].[4].property.[0].[0].arg[0].value.[2].modifier[6].arg[1].value", fallback: 80.0))
		})
#sourceLocation()
    }
}

extension ContentView {
    @_dynamicReplacement(for: disableButton) private var __preview__disableButton: Bool {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 48)
		from.count < 6 || to.count < 6
#sourceLocation()
    }
}

typealias ContentView = CabwayOfficial_WatchKit_Extension.ContentView
typealias ContentView_Preview = CabwayOfficial_WatchKit_Extension.ContentView_Preview
typealias LocationTestView = CabwayOfficial_WatchKit_Extension.LocationTestView
typealias LocationTestView_Preview = CabwayOfficial_WatchKit_Extension.LocationTestView_Preview
typealias Book = CabwayOfficial_WatchKit_Extension.Book
typealias Book_Preview = CabwayOfficial_WatchKit_Extension.Book_Preview
typealias Favourite = CabwayOfficial_WatchKit_Extension.Favourite
typealias Favourite_Preview = CabwayOfficial_WatchKit_Extension.Favourite_Preview
typealias Info = CabwayOfficial_WatchKit_Extension.Info
typealias Info_Preview = CabwayOfficial_WatchKit_Extension.Info_Preview
typealias NotificationsSend = CabwayOfficial_WatchKit_Extension.NotificationsSend
typealias NotificationsSend_Previews = CabwayOfficial_WatchKit_Extension.NotificationsSend_Previews