@_private(sourceFile: "ContentView.swift") import CabwayOfficial_WatchKit_Extension
import CoreLocation
import SwiftUI
import SwiftUI

extension NotificationsSend_Previews {
    @_dynamicReplacement(for: previews) private static var __preview__previews: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 446)
        AnyView(__designTimeSelection(NotificationsSend(), "#4421.[13].[0].property.[0].[0]"))
#sourceLocation()
    }
}

extension NotificationsSend {
    @_dynamicReplacement(for: body) private var __preview__body: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 434)
        AnyView(__designTimeSelection(VStack {
            __designTimeSelection(Text(__designTimeString("#4421.[12].[1].property.[0].[0].arg[0].value.[0].arg[0].value.[0].value", fallback: "Notification Demo")), "#4421.[12].[1].property.[0].[0].arg[0].value.[0]")
            __designTimeSelection(Button(action: { __designTimeSelection(self.setNotification(), "#4421.[12].[1].property.[0].[0].arg[0].value.[1].arg[0].value.[0]") }) {
                __designTimeSelection(Text(__designTimeString("#4421.[12].[1].property.[0].[0].arg[0].value.[1].arg[1].value.[0].arg[0].value.[0].value", fallback: "Set Notification!")), "#4421.[12].[1].property.[0].[0].arg[0].value.[1].arg[1].value.[0]")
            }, "#4421.[12].[1].property.[0].[0].arg[0].value.[1]")
        }, "#4421.[12].[1].property.[0].[0]"))
#sourceLocation()
    }
}

extension NotificationsSend {
    @_dynamicReplacement(for: setNotification()) private func __preview__setNotification() -> Void {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 428)
        let manager = LocalNotificationManager()
        __designTimeSelection(manager.addNotification(title: __designTimeString("#4421.[12].[0].[1].modifier[0].arg[0].value.[0].value", fallback: "This is a test reminder")), "#4421.[12].[0].[1]")
        __designTimeSelection(manager.schedule(), "#4421.[12].[0].[2]")
#sourceLocation()
    }
}

extension Info_Preview {
    @_dynamicReplacement(for: previews) private static var __preview__previews: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 363)
		AnyView(__designTimeSelection(Info(), "#4421.[11].[0].property.[0].[0]"))
#sourceLocation()
    }
}

extension Info {
    @_dynamicReplacement(for: body) private var __preview__body: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 342)
		AnyView(__designTimeSelection(VStack(alignment: .center){
			
			
			__designTimeSelection(Image (__designTimeString("#4421.[10].[1].property.[0].[0].arg[1].value.[0].arg[0].value.[0].value", fallback: "Check"))
				.resizable()
				.padding(.bottom)
				.scaledToFit()
				.frame(width: __designTimeFloat("#4421.[10].[1].property.[0].[0].arg[1].value.[0].modifier[3].arg[0].value", fallback: 100.0), height: __designTimeFloat("#4421.[10].[1].property.[0].[0].arg[1].value.[0].modifier[3].arg[1].value", fallback: 100.0))
				.clipShape(__designTimeSelection(Circle(), "#4421.[10].[1].property.[0].[0].arg[1].value.[0].modifier[4].arg[0].value"))
				.shadow(radius: __designTimeInteger("#4421.[10].[1].property.[0].[0].arg[1].value.[0].modifier[5].arg[0].value", fallback: 30)), "#4421.[10].[1].property.[0].[0].arg[1].value.[0]")
			
			__designTimeSelection(VStack(spacing: __designTimeFloat("#4421.[10].[1].property.[0].[0].arg[1].value.[1].arg[0].value", fallback: 5.0)) {
				__designTimeSelection(Text(__designTimeString("#4421.[10].[1].property.[0].[0].arg[1].value.[1].arg[1].value.[0].arg[0].value.[0].value", fallback: "Your taxy is coming, thank you."))
					.multilineTextAlignment(.center), "#4421.[10].[1].property.[0].[0].arg[1].value.[1].arg[1].value.[0]")
			}, "#4421.[10].[1].property.[0].[0].arg[1].value.[1]")
		}, "#4421.[10].[1].property.[0].[0]"))
#sourceLocation()
    }
}

extension Favourite_Preview {
    @_dynamicReplacement(for: previews) private static var __preview__previews: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 328)
		AnyView(__designTimeSelection(Group{
			__designTimeSelection(Favourite(), "#4421.[9].[0].property.[0].[0].arg[0].value.[0]")
			__designTimeSelection(Favourite()
				.previewDevice(__designTimeString("#4421.[9].[0].property.[0].[0].arg[0].value.[1].modifier[0].arg[0].value.[0].value", fallback: "Apple Watch Series 3 - 38mm")), "#4421.[9].[0].property.[0].[0].arg[0].value.[1]")
		}, "#4421.[9].[0].property.[0].[0]"))
#sourceLocation()
    }
}

extension Favourite {
    @_dynamicReplacement(for: body) private var __preview__body: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 259)
		
		AnyView(__designTimeSelection(VStack(alignment: .center) {
			
			__designTimeSelection(HStack{
				
				__designTimeSelection(Button(action: {
					__designTimeSelection(self.mode.wrappedValue.dismiss(), "#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[0].arg[0].value.[0]")
					
					
					//self.dataHolder.text = "HOME"
					
				}) {
					
						__designTimeSelection(HStack {
							
							__designTimeSelection(Image(systemName: __designTimeString("#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[0].arg[1].value.[0].arg[0].value.[0].arg[0].value.[0].value", fallback: "house.fill"))
								.foregroundColor(__designTimeSelection(Color.black, "#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[0].arg[1].value.[0].arg[0].value.[0].modifier[0].arg[0].value")), "#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[0].arg[1].value.[0].arg[0].value.[0]")
							
							__designTimeSelection(Text(__designTimeString("#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[0].arg[1].value.[0].arg[0].value.[1].arg[0].value.[0].value", fallback: "Home"))
								.foregroundColor(__designTimeSelection(Color.black, "#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[0].arg[1].value.[0].arg[0].value.[1].modifier[0].arg[0].value"))
								.font(.system(size: __designTimeInteger("#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[0].arg[1].value.[0].arg[0].value.[1].modifier[1].arg[0].value.arg[0].value", fallback: 8))), "#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[0].arg[1].value.[0].arg[0].value.[1]")
						}, "#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[0].arg[1].value.[0]")
						
					
				}
				.frame(height: __designTimeFloat("#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[0].modifier[0].arg[0].value", fallback: 45.0))
				.background(__designTimeSelection(Color.yellow, "#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[0].modifier[1].arg[0].value"))
				.cornerRadius(__designTimeInteger("#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[0].modifier[2].arg[0].value", fallback: 10)), "#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[0]")
				
				
				
				__designTimeSelection(Button(action: {__designTimeSelection(self.mode.wrappedValue.dismiss(), "#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[1].arg[0].value.[0]")}) {
					
						__designTimeSelection(HStack {
							__designTimeSelection(Image(systemName: __designTimeString("#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[1].arg[1].value.[0].arg[0].value.[0].arg[0].value.[0].value", fallback: "bag.fill"))
								.foregroundColor(__designTimeSelection(Color.black, "#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[1].arg[1].value.[0].arg[0].value.[0].modifier[0].arg[0].value")), "#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[1].arg[1].value.[0].arg[0].value.[0]")
							__designTimeSelection(Text(__designTimeString("#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[1].arg[1].value.[0].arg[0].value.[1].arg[0].value.[0].value", fallback: "Work"))
								.foregroundColor(__designTimeSelection(Color.black, "#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[1].arg[1].value.[0].arg[0].value.[1].modifier[0].arg[0].value"))
								.font(.system(size: __designTimeInteger("#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[1].arg[1].value.[0].arg[0].value.[1].modifier[1].arg[0].value.arg[0].value", fallback: 8))), "#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[1].arg[1].value.[0].arg[0].value.[1]")
						}, "#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[1].arg[1].value.[0]")
					
				}
				.frame(height: __designTimeFloat("#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[1].modifier[0].arg[0].value", fallback: 45.0))
				.background(__designTimeSelection(Color.yellow, "#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[1].modifier[1].arg[0].value"))
				.cornerRadius(__designTimeInteger("#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[1].modifier[2].arg[0].value", fallback: 10)), "#4421.[8].[6].property.[0].[0].arg[1].value.[0].arg[0].value.[1]")
			}
			.frame(height: nil), "#4421.[8].[6].property.[0].[0].arg[1].value.[0]")
			__designTimeSelection(TextField(__designTimeString("#4421.[8].[6].property.[0].[0].arg[1].value.[1].arg[0].value.[0].value", fallback: "Choose"), text: __designTimeSelection($choose, "#4421.[8].[6].property.[0].[0].arg[1].value.[1].arg[1].value")), "#4421.[8].[6].property.[0].[0].arg[1].value.[1]")
			__designTimeSelection(List{
				
				__designTimeSelection(Button(action: {__designTimeSelection(self.mode.wrappedValue.dismiss(), "#4421.[8].[6].property.[0].[0].arg[1].value.[2].arg[0].value.[0].arg[0].value.[0]")}){
				
					__designTimeSelection(Text(__designTimeString("#4421.[8].[6].property.[0].[0].arg[1].value.[2].arg[0].value.[0].arg[1].value.[0].arg[0].value.[0].value", fallback: "via cercata 1")), "#4421.[8].[6].property.[0].[0].arg[1].value.[2].arg[0].value.[0].arg[1].value.[0]")
				}, "#4421.[8].[6].property.[0].[0].arg[1].value.[2].arg[0].value.[0]")
				__designTimeSelection(Button(action: {__designTimeSelection(self.mode.wrappedValue.dismiss(), "#4421.[8].[6].property.[0].[0].arg[1].value.[2].arg[0].value.[1].arg[0].value.[0]")}){
					
					__designTimeSelection(Text(__designTimeString("#4421.[8].[6].property.[0].[0].arg[1].value.[2].arg[0].value.[1].arg[1].value.[0].arg[0].value.[0].value", fallback: "via cercata 2")), "#4421.[8].[6].property.[0].[0].arg[1].value.[2].arg[0].value.[1].arg[1].value.[0]")
				}, "#4421.[8].[6].property.[0].[0].arg[1].value.[2].arg[0].value.[1]")
				__designTimeSelection(Button(action: {__designTimeSelection(self.mode.wrappedValue.dismiss(), "#4421.[8].[6].property.[0].[0].arg[1].value.[2].arg[0].value.[2].arg[0].value.[0]")}){
					
					__designTimeSelection(Text(__designTimeString("#4421.[8].[6].property.[0].[0].arg[1].value.[2].arg[0].value.[2].arg[1].value.[0].arg[0].value.[0].value", fallback: "via cercata 3")), "#4421.[8].[6].property.[0].[0].arg[1].value.[2].arg[0].value.[2].arg[1].value.[0]")
				}, "#4421.[8].[6].property.[0].[0].arg[1].value.[2].arg[0].value.[2]")
				
			}, "#4421.[8].[6].property.[0].[0].arg[1].value.[2]")
		}, "#4421.[8].[6].property.[0].[0]"))
#sourceLocation()
    }
}

extension Book_Preview {
    @_dynamicReplacement(for: previews) private static var __preview__previews: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 232)
		AnyView(__designTimeSelection(Group{
			__designTimeSelection(Book(), "#4421.[7].[0].property.[0].[0].arg[0].value.[0]")
			__designTimeSelection(Book()
				.previewDevice(__designTimeString("#4421.[7].[0].property.[0].[0].arg[0].value.[1].modifier[0].arg[0].value.[0].value", fallback: "Apple Watch Series 3 - 38mm")), "#4421.[7].[0].property.[0].[0].arg[0].value.[1]")
		}, "#4421.[7].[0].property.[0].[0]"))
#sourceLocation()
    }
}

extension Book {
    @_dynamicReplacement(for: body) private var __preview__body: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 164)
		
		AnyView(__designTimeSelection(VStack(alignment: .center){
			
			__designTimeSelection(HStack(alignment: .bottom){
				
				__designTimeSelection(VStack(alignment: .leading){
					
					__designTimeSelection(Image(__designTimeString("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[0].arg[1].value.[0].arg[0].value.[0].value", fallback: "Taxi"))
						.resizable()
						.scaledToFit()
						.frame(width: __designTimeFloat("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[0].arg[1].value.[0].modifier[2].arg[0].value", fallback: 70.0), height: __designTimeFloat("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[0].arg[1].value.[0].modifier[2].arg[1].value", fallback: 70.0))
						.clipShape(__designTimeSelection(Circle(), "#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[0].arg[1].value.[0].modifier[3].arg[0].value"))
						//.shadow(radius: 10)
						.overlay(__designTimeSelection(Circle().stroke(__designTimeSelection(Color.white, "#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[0].arg[1].value.[0].modifier[4].arg[0].value.modifier[0].arg[0].value"), lineWidth: __designTimeInteger("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[0].arg[1].value.[0].modifier[4].arg[0].value.modifier[0].arg[1].value", fallback: 1)).imageScale(/*@START_MENU_TOKEN@*/.medium/*@END_MENU_TOKEN@*/), "#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[0].arg[1].value.[0].modifier[4].arg[0].value"))
					.transition(__designTimeSelection(AnyTransition.opacity.combined(with: .slide), "#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[0].arg[1].value.[0].modifier[5].arg[0].value")), "#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[0].arg[1].value.[0]")
					
					
					
					__designTimeSelection(Text(__designTimeString("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[0].arg[1].value.[1].arg[0].value.[0].value", fallback: "4 E"))
						.multilineTextAlignment(.leading), "#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[0].arg[1].value.[1]")
					__designTimeSelection(Text(__designTimeString("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[0].arg[1].value.[2].arg[0].value.[0].value", fallback: "1.2 KM"))
						.multilineTextAlignment(.leading), "#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[0].arg[1].value.[2]")
					
				}, "#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[0]")
				__designTimeSelection(Spacer(), "#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[1]")
				__designTimeSelection(VStack(alignment: .center){
					__designTimeSelection(Text(__designTimeString("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[2].arg[1].value.[0].arg[0].value.[0].value", fallback: "3"))
						.fontWeight(.semibold)
						.multilineTextAlignment(.trailing)
						
						.frame(width: __designTimeFloat("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[2].arg[1].value.[0].modifier[2].arg[0].value", fallback: 70.0), height: __designTimeFloat("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[2].arg[1].value.[0].modifier[2].arg[1].value", fallback: 120.0))
						.font(.system(size: __designTimeInteger("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[2].arg[1].value.[0].modifier[3].arg[0].value.arg[0].value", fallback: 80))), "#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[2].arg[1].value.[0]")
					
					__designTimeSelection(Text(__designTimeString("#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[2].arg[1].value.[1].arg[0].value.[0].value", fallback: "MINS")), "#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[2].arg[1].value.[1]")
						
					
				}, "#4421.[6].[0].property.[0].[0].arg[1].value.[0].arg[1].value.[2]")
				
			}, "#4421.[6].[0].property.[0].[0].arg[1].value.[0]")
			
			
			__designTimeSelection(VStack(alignment: .center){
				__designTimeSelection(Spacer(), "#4421.[6].[0].property.[0].[0].arg[1].value.[1].arg[1].value.[0]")
				__designTimeSelection(Button(action: {},label:{
					__designTimeSelection(VStack {
						__designTimeSelection(NavigationLink(destination: __designTimeSelection(Info(), "#4421.[6].[0].property.[0].[0].arg[1].value.[1].arg[1].value.[1].arg[1].value.[0].arg[0].value.[0].arg[0].value")) {
							__designTimeSelection(Text(__designTimeString("#4421.[6].[0].property.[0].[0].arg[1].value.[1].arg[1].value.[1].arg[1].value.[0].arg[0].value.[0].arg[1].value.[0].arg[0].value.[0].value", fallback: "Confirm"))
								.font(.caption)
								.fontWeight(.regular)
								.foregroundColor(__designTimeSelection(Color.black, "#4421.[6].[0].property.[0].[0].arg[1].value.[1].arg[1].value.[1].arg[1].value.[0].arg[0].value.[0].arg[1].value.[0].modifier[2].arg[0].value"))
								.multilineTextAlignment(.center)
								.lineLimit(nil)
								.padding()
								.frame(height: nil), "#4421.[6].[0].property.[0].[0].arg[1].value.[1].arg[1].value.[1].arg[1].value.[0].arg[0].value.[0].arg[1].value.[0]")
						}, "#4421.[6].[0].property.[0].[0].arg[1].value.[1].arg[1].value.[1].arg[1].value.[0].arg[0].value.[0]")
					}, "#4421.[6].[0].property.[0].[0].arg[1].value.[1].arg[1].value.[1].arg[1].value.[0]")
					
				})
					.frame(height: __designTimeFloat("#4421.[6].[0].property.[0].[0].arg[1].value.[1].arg[1].value.[1].modifier[0].arg[0].value", fallback: 40.0))
					.background(__designTimeSelection(Color.yellow, "#4421.[6].[0].property.[0].[0].arg[1].value.[1].arg[1].value.[1].modifier[1].arg[0].value"))
					.cornerRadius(__designTimeInteger("#4421.[6].[0].property.[0].[0].arg[1].value.[1].arg[1].value.[1].modifier[2].arg[0].value", fallback: 10)), "#4421.[6].[0].property.[0].[0].arg[1].value.[1].arg[1].value.[1]")
			}, "#4421.[6].[0].property.[0].[0].arg[1].value.[1]")
			
		}, "#4421.[6].[0].property.[0].[0]"))
#sourceLocation()
    }
}

extension LocationTestView_Preview {
    @_dynamicReplacement(for: previews) private static var __preview__previews: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 154)
		AnyView(__designTimeSelection(LocationTestView(), "#4421.[5].[0].property.[0].[0]"))
#sourceLocation()
    }
}

extension LocationTestView {
    @_dynamicReplacement(for: body) private var __preview__body: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 141)
		
		AnyView(__designTimeSelection(VStack {
			__designTimeSelection(List{
				__designTimeSelection(Text("\(__designTimeString("#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[0].value", fallback: "Latitude: "))\(__designTimeSelection(self.latitude, "#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[1].value.arg[0].value"))\(__designTimeString("#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0].arg[0].value.[2].value", fallback: ""))"), "#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[0]")
				__designTimeSelection(Text("\(__designTimeString("#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[1].arg[0].value.[0].value", fallback: "Longitude: "))\(__designTimeSelection(self.longitude, "#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[1].arg[0].value.[1].value.arg[0].value"))\(__designTimeString("#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[1].arg[0].value.[2].value", fallback: ""))"), "#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[1]")
				__designTimeSelection(Text("\(__designTimeString("#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[2].arg[0].value.[0].value", fallback: "Placemark: "))\(__designTimeSelection(self.placemark, "#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[2].arg[0].value.[1].value.arg[0].value"))\(__designTimeString("#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[2].arg[0].value.[2].value", fallback: ""))"), "#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[2]")
				__designTimeSelection(Text("\(__designTimeString("#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[3].arg[0].value.[0].value", fallback: "Status: "))\(__designTimeSelection(self.status, "#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[3].arg[0].value.[1].value.arg[0].value"))\(__designTimeString("#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[3].arg[0].value.[2].value", fallback: ""))"), "#4421.[4].[5].property.[0].[0].arg[0].value.[0].arg[0].value.[3]")
			}, "#4421.[4].[5].property.[0].[0].arg[0].value.[0]")
		}, "#4421.[4].[5].property.[0].[0]"))
#sourceLocation()
    }
}

extension LocationTestView {
    @_dynamicReplacement(for: status) private var __preview__status: String {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 136)
 return("\(__designTimeString("#4421.[4].[4].property.[0].[0].[0].[0].value", fallback: ""))\(__designTimeSelection(String(describing: __designTimeSelection(lm.status, "#4421.[4].[4].property.[0].[0].[0].[1].value.arg[0].value.arg[0].value")), "#4421.[4].[4].property.[0].[0].[0].[1].value.arg[0].value"))\(__designTimeString("#4421.[4].[4].property.[0].[0].[0].[2].value", fallback: ""))")
#sourceLocation()
    }
}

extension LocationTestView {
    @_dynamicReplacement(for: placemark) private var __preview__placemark: String {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 135)
 return("\(__designTimeString("#4421.[4].[3].property.[0].[0].[0].[0].value", fallback: ""))\(lm.placemark?.description ?? "XXX")\(__designTimeString("#4421.[4].[3].property.[0].[0].[0].[2].value", fallback: ""))")
#sourceLocation()
    }
}

extension LocationTestView {
    @_dynamicReplacement(for: longitude) private var __preview__longitude: String {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 134)
 return("\(__designTimeString("#4421.[4].[2].property.[0].[0].[0].[0].value", fallback: ""))\(lm.location?.longitude ?? 0)\(__designTimeString("#4421.[4].[2].property.[0].[0].[0].[2].value", fallback: ""))")
#sourceLocation()
    }
}

extension LocationTestView {
    @_dynamicReplacement(for: latitude) private var __preview__latitude: String {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 133)
 return("\(__designTimeString("#4421.[4].[1].property.[0].[0].[0].[0].value", fallback: ""))\(lm.location?.latitude ?? 0)\(__designTimeString("#4421.[4].[1].property.[0].[0].[0].[2].value", fallback: ""))")
#sourceLocation()
    }
}

extension ContentView_Preview {
    @_dynamicReplacement(for: previews) private static var __preview__previews: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 122)
		AnyView(__designTimeSelection(ContentView(), "#4421.[3].[0].property.[0].[0]"))
#sourceLocation()
    }
}

extension ContentView {
    @_dynamicReplacement(for: body) private var __preview__body: some View {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 53)
		
		AnyView(__designTimeSelection(VStack {
			//TextField("From", text: $from)
			//.padding(.top, 40.0)
			
			__designTimeSelection(Button(action: {}, label: {
				__designTimeSelection(NavigationLink(destination: __designTimeSelection(Favourite(), "#4421.[2].[4].property.[0].[0].arg[0].value.[0].arg[1].value.[0].arg[0].value")) {
					
					__designTimeSelection(HStack {
						     //DOVE VA MESSO???	//ContentView().environmentObject(ExampleDataHolder())
						__designTimeSelection(Image(systemName: __designTimeString("#4421.[2].[4].property.[0].[0].arg[0].value.[0].arg[1].value.[0].arg[1].value.[0].arg[0].value.[0].arg[0].value.[0].value", fallback: "location.fill")), "#4421.[2].[4].property.[0].[0].arg[0].value.[0].arg[1].value.[0].arg[1].value.[0].arg[0].value.[0]")
						__designTimeSelection(TextField(__designTimeString("#4421.[2].[4].property.[0].[0].arg[0].value.[0].arg[1].value.[0].arg[1].value.[0].arg[0].value.[1].arg[0].value.[0].value", fallback: "From"), text: __designTimeSelection($from, "#4421.[2].[4].property.[0].[0].arg[0].value.[0].arg[1].value.[0].arg[1].value.[0].arg[0].value.[1].arg[1].value")), "#4421.[2].[4].property.[0].[0].arg[0].value.[0].arg[1].value.[0].arg[1].value.[0].arg[0].value.[1]")
					
					}, "#4421.[2].[4].property.[0].[0].arg[0].value.[0].arg[1].value.[0].arg[1].value.[0]")
				}.navigationBarTitle(__designTimeString("#4421.[2].[4].property.[0].[0].arg[0].value.[0].arg[1].value.[0].modifier[0].arg[0].value.[0].value", fallback: "Cabway")), "#4421.[2].[4].property.[0].[0].arg[0].value.[0].arg[1].value.[0]")
				
				
			}).padding(.top, __designTimeFloat("#4421.[2].[4].property.[0].[0].arg[0].value.[0].modifier[0].arg[1].value", fallback: 40.0)), "#4421.[2].[4].property.[0].[0].arg[0].value.[0]")
			
			
			__designTimeSelection(Button(action: {
				
				
			}, label: {
				__designTimeSelection(NavigationLink(destination: __designTimeSelection(Favourite(), "#4421.[2].[4].property.[0].[0].arg[0].value.[1].arg[1].value.[0].arg[0].value")) {
					__designTimeSelection(TextField(__designTimeString("#4421.[2].[4].property.[0].[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[0].arg[0].value.[0].value", fallback: "To"), text: __designTimeSelection($to, "#4421.[2].[4].property.[0].[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[0].arg[1].value")), "#4421.[2].[4].property.[0].[0].arg[0].value.[1].arg[1].value.[0].arg[1].value.[0]")
					
					
				}.navigationBarTitle(__designTimeString("#4421.[2].[4].property.[0].[0].arg[0].value.[1].arg[1].value.[0].modifier[0].arg[0].value.[0].value", fallback: "Cabway")), "#4421.[2].[4].property.[0].[0].arg[0].value.[1].arg[1].value.[0]")
				
			}), "#4421.[2].[4].property.[0].[0].arg[0].value.[1]")
			
			
			__designTimeSelection(Button(action: {}, label: {
				
				//Mostro la struct Book
				__designTimeSelection(NavigationLink(destination: __designTimeSelection(Book(), "#4421.[2].[4].property.[0].[0].arg[0].value.[2].arg[1].value.[0].arg[0].value")) {
					__designTimeSelection(Text(__designTimeString("#4421.[2].[4].property.[0].[0].arg[0].value.[2].arg[1].value.[0].arg[1].value.[0].arg[0].value.[0].value", fallback: "GO"))
						.fontWeight(.bold)
						.multilineTextAlignment(.center)
						.frame(width: __designTimeFloat("#4421.[2].[4].property.[0].[0].arg[0].value.[2].arg[1].value.[0].arg[1].value.[0].modifier[2].arg[0].value", fallback: 80.0), height: __designTimeFloat("#4421.[2].[4].property.[0].[0].arg[0].value.[2].arg[1].value.[0].arg[1].value.[0].modifier[2].arg[1].value", fallback: 80.0)), "#4421.[2].[4].property.[0].[0].arg[0].value.[2].arg[1].value.[0].arg[1].value.[0]")
				}.navigationBarTitle(__designTimeString("#4421.[2].[4].property.[0].[0].arg[0].value.[2].arg[1].value.[0].modifier[0].arg[0].value.[0].value", fallback: "Cabway")).accentColor(.yellow), "#4421.[2].[4].property.[0].[0].arg[0].value.[2].arg[1].value.[0]")
				
				//Per richiamare una funzione si fa cosi
			})//.onAppear{getPosition(userLatitude: "37.78583400",userLongitude: userLongitude)}
				
				
				.font(.title)
				.background(__designTimeSelection(disableButton, "#4421.[2].[4].property.[0].[0].arg[0].value.[2].modifier[1].arg[0].value.if") ? __designTimeSelection(Color.gray, "#4421.[2].[4].property.[0].[0].arg[0].value.[2].modifier[1].arg[0].value.then") : __designTimeSelection(Color.yellow, "#4421.[2].[4].property.[0].[0].arg[0].value.[2].modifier[1].arg[0].value.else"))
				.disabled(__designTimeSelection(disableButton, "#4421.[2].[4].property.[0].[0].arg[0].value.[2].modifier[2].arg[0].value.if") ? __designTimeBoolean("#4421.[2].[4].property.[0].[0].arg[0].value.[2].modifier[2].arg[0].value.then", fallback: true) : __designTimeBoolean("#4421.[2].[4].property.[0].[0].arg[0].value.[2].modifier[2].arg[0].value.else", fallback: false))
				.opacity(__designTimeSelection(disableButton, "#4421.[2].[4].property.[0].[0].arg[0].value.[2].modifier[3].arg[0].value.if") ? __designTimeFloat("#4421.[2].[4].property.[0].[0].arg[0].value.[2].modifier[3].arg[0].value.then", fallback: 0.15) : __designTimeInteger("#4421.[2].[4].property.[0].[0].arg[0].value.[2].modifier[3].arg[0].value.else", fallback: 1))
				.foregroundColor(__designTimeSelection(disableButton, "#4421.[2].[4].property.[0].[0].arg[0].value.[2].modifier[4].arg[0].value.if") ? .gray : .black)
				
				.cornerRadius(__designTimeInteger("#4421.[2].[4].property.[0].[0].arg[0].value.[2].modifier[5].arg[0].value", fallback: 50))
				.frame(width: __designTimeFloat("#4421.[2].[4].property.[0].[0].arg[0].value.[2].modifier[6].arg[0].value", fallback: 80.0), height: __designTimeFloat("#4421.[2].[4].property.[0].[0].arg[0].value.[2].modifier[6].arg[1].value", fallback: 80.0)), "#4421.[2].[4].property.[0].[0].arg[0].value.[2]")
		}, "#4421.[2].[4].property.[0].[0]"))
#sourceLocation()
    }
}

extension ContentView {
    @_dynamicReplacement(for: disableButton) private var __preview__disableButton: Bool {
        #sourceLocation(file: "/Users/mauriziominieri/Projects/CabwayOfficial/CabwayOfficial WatchKit Extension/ContentView.swift", line: 48)
		from.count < 6 || to.count < 6
#sourceLocation()
    }
}

typealias ContentView = CabwayOfficial_WatchKit_Extension.ContentView
typealias ContentView_Preview = CabwayOfficial_WatchKit_Extension.ContentView_Preview
typealias LocationTestView = CabwayOfficial_WatchKit_Extension.LocationTestView
typealias LocationTestView_Preview = CabwayOfficial_WatchKit_Extension.LocationTestView_Preview
typealias Book = CabwayOfficial_WatchKit_Extension.Book
typealias Book_Preview = CabwayOfficial_WatchKit_Extension.Book_Preview
typealias Favourite = CabwayOfficial_WatchKit_Extension.Favourite
typealias Favourite_Preview = CabwayOfficial_WatchKit_Extension.Favourite_Preview
typealias Info = CabwayOfficial_WatchKit_Extension.Info
typealias Info_Preview = CabwayOfficial_WatchKit_Extension.Info_Preview
typealias NotificationsSend = CabwayOfficial_WatchKit_Extension.NotificationsSend
typealias NotificationsSend_Previews = CabwayOfficial_WatchKit_Extension.NotificationsSend_Previews